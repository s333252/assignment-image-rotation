#include "image.h"
#include "bmp.h"
#include "rotate.h"
#include <stdio.h>
#include <stdlib.h>

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning
    if (argc != 3) {
        fprintf(stderr, "Invalid argument count\n");
        return 1;
    }

    struct image source;
    enum read_status rStatus = read_bmp(argv[1], &source);
    if (rStatus != READ_OK) {
        fprintf(stderr, "Error while loading BMP: error code %d\n", rStatus);
        return 1;
    }

    struct image result = rotate(source);
    enum write_status wStatus = write_bmp(argv[2], &result);
    if (wStatus != WRITE_OK) {
        fprintf(stderr, "Error while creating BMP: error code %d\n", wStatus);
        return 1;
    }

    destroy_image(&source);
    destroy_image(&result);

    return 0;
}
