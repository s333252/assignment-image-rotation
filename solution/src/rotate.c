#include "image.h"
#include <stdlib.h>

static struct pixel get_pixel(const struct image* img, uint64_t w, uint64_t h) {
  return img->data[img->width*h+w];
}

static void set_pixel(struct image* img, uint64_t w, uint64_t h, const struct pixel value) {
  img->data[img->width*h+w] = value;
}

struct image rotate(struct image const src ) {
  struct image res;
  res.width = src.height;
  res.height = src.width;
  res.data = malloc(res.width*res.height*sizeof(struct pixel));
  if (res.data == NULL) {
    res.width = 0;
    res.height = 0;
    return res;
  }

  for (uint64_t i = 0; i < src.width; i++) {
    for (uint64_t j = 0; j < src.height; j++) {
      set_pixel(&res, src.height - j - 1, i, get_pixel(&src, i, j));
    }
  }

  return res;
}
