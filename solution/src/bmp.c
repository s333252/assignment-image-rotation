#include "image.h"
#include "bmp.h"
#include <stdio.h>
#include <stdlib.h>

struct __attribute__((packed)) bmp_header
{
  uint16_t bfType;
  uint32_t  bfileSize;
  uint32_t bfReserved;
  uint32_t bOffBits;
  uint32_t biSize;
  uint32_t biWidth;
  uint32_t  biHeight;
  uint16_t  biPlanes;
  uint16_t biBitCount;
  uint32_t biCompression;
  uint32_t biSizeImage;
  uint32_t biXPelsPerMeter;
  uint32_t biYPelsPerMeter;
  uint32_t biClrUsed;
  uint32_t  biClrImportant;
};

static size_t row_padding(uint64_t width) {
  return ((24 * width + 31) / 32) * 4 - 3*width;
}

static enum read_status check_header(struct bmp_header header) {
  if (header.bfType != 0x4D42) {
    return READ_INVALID_SIGNATURE;
  }
  if (header.biBitCount != 24) {
    return READ_INVALID_BITS;
  }
  if (header.biCompression != 0) {
    return READ_COMPRESSION_UNSUPPORTED;
  }
  return READ_OK;
}

static struct bmp_header make_header(uint32_t width, uint32_t height, uint32_t padding) {
  const uint32_t image_size = height*(3*width + padding);
  struct bmp_header res = {
    .bfType = 0x4D42,
    .bfileSize = 54 + image_size,
    .bfReserved = 0,
    .bOffBits = 54,
    .biSize = 40,
    .biWidth = width,
    .biHeight = height,
    .biPlanes = 1,
    .biBitCount = 24,
    .biCompression = 0,
    .biSizeImage = image_size,
    .biXPelsPerMeter = 2835,
    .biYPelsPerMeter = 2835,
    .biClrUsed = 0,
    .biClrImportant = 0,
  };
  return res;
}

enum read_status from_bmp( FILE* in, struct image* img ) {
  struct bmp_header header;
  if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) {
    return READ_IO_FAILURE;
  }
  
  enum read_status header_status = check_header(header);
  if (header_status != READ_OK) {
    return header_status;
  }

  struct image res;
  make_image(&res, header.biWidth, header.biHeight);
  if (res.data == NULL) {
    return READ_MALLOC_FAILURE;
  }

  const size_t padding = row_padding(header.biWidth);
  struct pixel* imgDataPtr = res.data;
  for (uint32_t r = 0; r < header.biHeight; r++) {
    if (fread(imgDataPtr, sizeof(struct pixel), header.biWidth, in) != header.biWidth) {
      destroy_image(&res);
      return READ_IO_FAILURE;
    }
    // Skip padding
    if (fseek(in, (long)padding, SEEK_CUR) != 0) {
      destroy_image(&res);
      return READ_IO_FAILURE;
    }
    imgDataPtr += header.biWidth;
  }

  *img = res;
  return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ) {
  const size_t padding = row_padding(img->width);

  struct bmp_header header = make_header(img->width, img->height, padding);
  if (fwrite(&header, sizeof(header), 1, out) < 1) {
    return WRITE_IO_ERROR;
  }

  struct pixel* imgDataPtr = img->data;
  for (int r = 0; r < img->height; r++) {
    if (fwrite(imgDataPtr, sizeof(struct pixel), img->width, out) != img->width) {
      return WRITE_IO_ERROR;
    }
    const char temp[4] = {0};
    if (fwrite(&temp, padding, 1, out) != 1) {
      return WRITE_IO_ERROR;
    }
    imgDataPtr += img->width;
  }

  return WRITE_OK;
}

enum read_status read_bmp(char const* path, struct image* img) {
  FILE* file = fopen(path, "rb");
  if (file == NULL) {
    return READ_IO_FAILURE;
  }
  enum read_status status = from_bmp(file, img);
  fclose(file);
  return status;
}

enum write_status write_bmp(char const* path, struct image const* img) {
  FILE* file = fopen(path, "wb");
  if (file == NULL) {
    return WRITE_ERROR;
  }
  enum write_status status = to_bmp(file, img);
  fclose(file);
  return status;
}
