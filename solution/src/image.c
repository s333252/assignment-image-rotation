#include "image.h"
#include <stdlib.h>

void make_image(struct image* img, uint64_t width, uint64_t height) {
  img->width = width;
  img->height = height;
  img->data = (struct pixel*)malloc(sizeof(struct pixel) * width * height);
}

void destroy_image(struct image* img) {
  if (img->data != NULL) {
    free(img->data);
  }
}
