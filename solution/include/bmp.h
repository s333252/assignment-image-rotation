#pragma once
#include "image.h"
#include <stdint.h>
#include <stdio.h>


/*  deserializer   */
enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_COMPRESSION_UNSUPPORTED,
  READ_MALLOC_FAILURE,
  READ_IO_FAILURE
  /* коды других ошибок  */
};

enum read_status from_bmp( FILE* in, struct image* img );

/*  serializer   */
enum  write_status  {
  WRITE_OK = 0,
  WRITE_IO_ERROR,
  WRITE_ERROR
  /* коды других ошибок  */
};

enum write_status to_bmp( FILE* out, struct image const* img );

enum read_status read_bmp(char const* path, struct image* img);
enum write_status write_bmp(char const* path, struct image const* img);
